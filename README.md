# Personal notes on book i've read

## List

* [Zero To One](https://github.com/gmolveau/books_personal_notes/blob/master/zero_to_one.md) - Peter THIEL
* [Goodbye, Things](https://github.com/gmolveau/books_personal_notes/blob/master/goodbye_things.md) - Fumio SASAKI
* [97 Things Every Programmer Should Know](https://github.com/gmolveau/books_personal_notes/blob/master/97_things_every_programmer_should_know.md) - Kevlin HENNEY
* [Superforcasting](https://github.com/gmolveau/books_personal_notes/blob/master/superforcasting.md) - Philip TETLOCK x Dan GARDNER
